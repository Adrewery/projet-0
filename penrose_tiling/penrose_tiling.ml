#load "graphics.cma";;
open Graphics;;

(* use to delay operations *)
#load "unix.cma";;

(* Graph initialisation *)
close_graph();;
open_graph " 1000x800-0+0";;

(* Our types and variables *)
let phi = (1.0 +. sqrt(5.0)) /. 2.0;;

(* types defining triangles *)
(** Shapes of golden triangle **)
type shape = Acute | Obtuse;;
(** Coordinate of triangle point in float for more precision **)
type point = {x : float; y : float};;
(** Type of triangle **)
type triangle = {shape : shape; points : point array};;

(** Build a golden triangle
    		- shape : Acute or Obtuse
    		- coefficient : double, proportionality coefficient of 
    		triangle size
 **)
let build_triangle shape coefficient = 
  if coefficient <= 0.0 then 
    failwith "build_triangle : negative coefficient";
  match shape with
  |Acute 	-> {shape = Acute; 	
              points = [|{x = 0.0; y = 0.0};
                         {x = sqrt((coefficient *. phi) 
                                   *. (coefficient *. phi) 
                                   -. (coefficient *. coefficient /. 4.0));
                          y = coefficient /. 2.0};
                         {x = 0.0; y = coefficient}|]};
  |Obtuse -> {shape = Obtuse; 
              points = [|{x = 0.0; y = 0.0};
                         {x = coefficient *. phi; y = 0.0};
                         {x = coefficient *. phi /. 2.0; 
                          y = coefficient 
                              *. (sqrt (1.0 -. (phi *. phi)/. 4.0))}|]};;

(* Drawing functions *)
(** Convert the triangle's points coordinate into int : 
    	(int * int) array
    		- triangle : triangle
 **)
let int_triangle_points triangle = 
  let int_points = [|(0,0);(0,0);(0,0)|] in
  for i = 0 to 2 do
    let triangle_point = triangle.points.(i) in
    int_points.(i) <- ((int_of_float triangle_point.x), 
                       (int_of_float triangle_point.y));
  done;
  int_points;;

(** Draw the triangle into the graphic window
    		- triangle : triangle
 **)
let draw triangle = 
  set_color black;
  set_line_width 2;
  let points = (int_triangle_points triangle) in
  for i = 0 to 2 do
    moveto (fst (points.(i))) 
      (snd (points.(i)));
    lineto (fst (points.((i+1) mod 3))) 
      (snd (points.((i+1) mod 3)));
  done;
  if triangle.shape = Acute then 
    (set_color blue)
  else 
    (set_color yellow);
  fill_poly points;;

(** Compute a point on the line creating by point1 and point2 : point
    	- point1 : point, first point
    	- point2 : point, second point
 **)
let compute_point point1 point2 =
  let new_point = { x=(point1.x +. (point2.x -. point1.x) /. phi); 
                    y=(point1.y +. (point2.y -. point1.y) /. phi)}
  in
  new_point;;

(** Divide triangle of generation n into generation n-1 triangles 
    	- generation : int, generation of the biggest golden triangle 
    		-> condition : generation >= 0
    	- triangle : triangle, a golden triangle create with the
    				 function 'build-triangle'
 **)
let rec divide generation triangle =
  if generation < 0 then failwith "divide : impossible generation";
  Unix.sleepf 0.01;
  if generation = 0 then 
    begin
      draw triangle;
    end
  else 
    begin
      let pointA = triangle.points.(0)
      and pointB = triangle.points.(1)
      and pointC = triangle.points.(2)
      in
      let pointD = (compute_point pointA pointB) in
      if triangle.shape = Obtuse then 
        begin
          divide (generation - 1)
            {shape = Acute; 
             points = [|pointC; pointA; pointD|]};
          divide (generation - 1)
            {shape = Obtuse;
             points = [|pointB; pointC; pointD|]};
        end 
      else 
        begin
          let pointE = (compute_point pointB pointC) in
          divide (generation - 1)
            {shape = Obtuse;
             points = [|pointB; pointE; pointD|]};
          divide (generation - 1)
            {shape = Acute;
             points = [|pointE; pointA; pointD|]};
          divide (generation - 1)
            {shape = Acute;
             points = [|pointC; pointA; pointE|]};
        end;
    end;;

(* DEMO *)

let wait() = 
	print_newline();
	prerr_string "Press enter to continue ...";
	prerr_newline ();
	read_line();;
	
print_string "Divide an ACUTE golden triangle by 5 generations.";;
divide 5 (build_triangle Acute 500.0);;

Unix.sleep 3;;
print_newline();;
clear_graph();

print_string "Divide an OBTUSE golden triangle by 3 generations.";;
divide 3 (build_triangle Obtuse 500.0);;

wait();;
close_graph();;
