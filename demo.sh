#Clear the command console.
clear

echo "		Demonstration of Project-0"
echo "by Kerian THUILLIER and Alexandre DREWERY"

cd penrose_tiling
echo "Start the demonstration of the penrose tiling project."
echo "	Main Function :"
#Start the demonstration of main function of the penrose tiling module.
ocaml penrose_tiling.ml
echo "\n	Extended Function :"
#Start the demonstration of extended function of the 
#penrose tiling module.
ocaml penrose_tiling_extended.ml

cd ../hanoi_towers
echo "Start the demonstration of the hanoi's towers project."
echo "	Main Functions :"
#Start the demonstration of main function of the hanoi's towers module.
ocaml hanoi_towers.ml
read -p "Press enter to create a pdf file containing the graph."
echo "Create a graph based on the data."
#Compile the R script.
R -f hanoi.r
#Remove data file from the disk.
rm hanoi.data
#Open the new created pdf.
evince hanoi_data.pdf
read -p "Press enter to continue ..."
clear
echo "	Extended Functions :"
#Start the demonstration of extended function of the
#hanoi's towers module.
ocaml hanoi_towers_extended.ml
read -p "Press enter to create a pdf file containing the graph."
#Compile the R script.
R -f hanoi_extended.r
#Remove data files from the disk.
rm hanoi_P.data
rm hanoi_Q.data
#Open the new created pdf.
evince hanoi_compared_data.pdf

cd ..
clear
echo "You have generated two pdf files :"
echo "	- hanoi_towers/hanoi_data.pdf ;"
echo "	- hanoi_towers/hanoi_compared_data.pdf ."

echo "\nEnd of the demonstration."
